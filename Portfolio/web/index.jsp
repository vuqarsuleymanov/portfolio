<%-- 
    Document   : index
    Created on : Oct 20, 2014, 10:06:03 PM
    Author     : Javel Group
--%>

<%@page import="az.com.entity.Services"%>
<%@page import="az.com.entity.Team"%>
<%@page import="java.util.ResourceBundle"%>
<%@page import="az.com.Vuqar.GetCookie"%>
<%@page import="az.com.Vuqar.Dao"%>
<%@page import="az.com.entity.Portfolio"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    //String lang = GetCookie.getCookieByName(request.getCookies(), "lang");
    ResourceBundle resource = ResourceBundle.getBundle("az/com/lang/lang_" + "en");
%>
<!DOCTYPE html>
<!-- saved from url=(0051) -->
<html lang="en" class=" js cssanimations csstransforms csstransforms3d csstransitions">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <!-- Basic Page Info and Settings -->
        <meta charset="utf-8">
        <title>Javel Group</title>
        <meta name="description" content="Javel Group - We believe in what we do.">
        <meta name="author" content="Javel Group">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

        <!-- Google Fonts -->
        <link href="files/css" rel="stylesheet" type="text/css">

        <!-- Styles and Scripts -->
        <link rel="stylesheet" href="files/animate.css">
        <link rel="stylesheet" href="files/style.css">
        <script src="files/scripts.js"></script>

    </head>
    <body>
        <!-- Site Header -->
        <header id="site-header" class="site-header">
            <!-- Large Container -->
            <div class="large-container">
                <!-- Site Logo -->
                <h1 class="site-logo">
                    <a href="index.jsp">
                        <img src="files/logo.png" alt="Emerald">
                    </a>
                </h1>
                <!-- End Site Logo -->
                <!-- Site Navigation -->
                <nav class="site-navigation">
                    <ul>
                        <li>
                            <a href="#" class="home-link"><%=resource.getString("menu.home")%></a>
                        </li>
                        <li>
                            <a href="#portfolio"><%=resource.getString("menu.portfolio")%></a>
                        </li>
                        <li>
                            <a href="#services"><%=resource.getString("menu.services")%></a>
                        </li>
                        <li>
                            <a href="#about-us"><%=resource.getString("menu.about")%></a>
                        </li>
                        <li>
                            <a href="#news"><%=resource.getString("menu.news")%></a>
                        </li>
                        <li>
                            <a href="#contact"><%=resource.getString("menu.contact")%></a>
                        </li>
                    </ul>
                </nav>
                <!-- End Site Navigation -->
                <div class="small-nav"><div></div></div></div>
            <!-- End Large Container -->
        </header>
        <!-- End Site Header -->
        <!-- Main Page Content -->
        <div role="main">
            <!-- Main Banner -->
            <div class="main-banner">
                <img src="files/main-banner.png" alt="" class="animated rotateIn delay-0-5s">
                <!-- Slogan -->
                <h1 class="slogan animated bounceInRight delay-1s duration-2s">
                    Javel<br>Group
                </h1>
                <p class="sub-slogan animated bounceInRight delay-1-5s duration-2s">
                    We believe in what we do.  
                </p>
                <!-- End Slogan -->
            </div>
            <!-- End Main Banner -->
            <!-- Portfolio -->
            <section id="portfolio" class="portfolio macro-block">
                <h1 class="macro-block-heading">
                    Portfolio
                </h1>
                <!-- Macro Block Content -->
                <div class="macro-block-content">
                    <!-- Container -->
                    <div class="container">
                        <!-- Filter -->
                        <ul class="filter sixteen columns animated fadeInDown">
                            <li>Filter:</li>
                            <li class="active-filter">
                                <a href="#" data-filter="all">All</a>
                            </li>
                            <li>
                                <a href="#" data-filter="web">Web Projects</a>
                            </li>
                            <li>
                                <a href="#" data-filter="desktop">Desktop Projects</a>
                            </li>
                        </ul>
                        <!-- End Filter -->
                    </div>
                    <!-- End Container -->
                    <!-- Tiles -->
                    <ul class="tiles container isotope" style="position: relative; overflow: hidden; height: 880px;">
                        <!-- Tile -->
                        <% for (Portfolio p : Dao.listPortfolio("en")) {%>
                        <li class="one-third column isotope-item" data-type="<%=p.getType()%>" style="position: absolute; left: 0px; top: 0px; transform: translate3d(0px, 0px, 0px);">
                            <div class="item-wrap animated fadeInLeft" style="opacity: 0;">
                                <a href="<%=p.getImage()%>" class="fancybox" data-rel="gallery-1" rel="gallery-1">
                                    <img src="<%=p.getImage()%>" alt="<%=p.getDescription()%>">
                                    <div class="overlay">
                                        <div class="overlay-inner"><p class="zoom-icon"></p>
                                            <p class="project-title"><%=p.getDescription()%></p>
                                            <p class="tags"><%=p.getType().toUpperCase()%></p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </li>
                        <% } %>
                        <!-- End Tile -->
                    </ul>
                    <!-- End Tiles -->
                </div>
                <!-- End Macro Block Content -->
            </section>
            <!-- End Portfolio -->
            <!-- Services -->
            <section id="services" class="services macro-block">
                <h1 class="macro-block-heading">
                    Our Services
                </h1>
                <!-- Macro Block Content -->
                <div class="macro-block-content">
                    <!-- Container -->
                    <div class="container services-details">
                        <h3 class="basic-text">
                            Every website is unique but the process is always more or less as follows.
                        </h3>
                        <!-- Service -->
                        <% for (Services p : az.com.Javanshir.Dao.listServices("en")) {%>
                        <div class="one-third column animated bounceInLeft">
                            <h3 class="content-heading-with-icon <%=p.getIcon()%>">
                                <%=p.getService_name()%>
                            </h3>
                            <p class="basic-text">
                                <%=p.getDescription()%>
                            </p>
                        </div>
                        <% } %>
                        <!-- End Service -->
                    </div>
                    <!-- End Container -->
                    <!-- Our Work Process -->
                    <div id="work-process" class="container">
                        <h2 class="macro-block-heading-sub sixteen columns">
                            <span>Our Work Process</span>
                        </h2>
                        <h3 class="basic-text">
                            We’re passionate about digital technology and taking on new creative challenges.
                        </h3>
                        <br>
                        <br>
                        <!-- Illustration -->
                        <div class="process-img full-width-img sixteen columns animated bounceIn">
                            <img src="files/process.png" alt="Our Work Process">
                        </div>
                        <!-- End Illustration -->
                    </div>
                    <!-- End Our Work Process -->
                </div>
                <!-- End Macro Block Content -->
            </section>
            <!-- End Services -->
            <!-- About Us -->
            <section id="about-us" class="about-us macro-block">
                <h1 class="macro-block-heading">
                    About Us
                </h1>
                <!-- Macro Block Content -->
                <div class="macro-block-content">
                    <!-- Container -->
                    <div class="container about-details">
                        <!-- Illustration -->
                        <div class="office-img full-width-img sixteen columns animated fadeInUp">
                            <img src="files/office.jpg" alt="Our Team at Work At Office">
                        </div>
                        <!-- End Illustration -->
                        <!-- Main Text -->
                        <div class="eight columns left-text animated bounceInRight">
                            <h3 class="content-heading">
                                Amazing Experience. On any Device
                            </h3>
                        </div>
                        <div class="eight columns right-text animated bounceInLeft">
                            <h3 class="content-heading">
                                Stunning Animation. Like Never Before
                            </h3>
                        </div>
                        <!-- End Main Text -->
                    </div>
                    <!-- End Container -->
                    <div class="container">
                        <h2 class="macro-block-heading-sub sixteen columns">
                            <span>Meet Our Team</span>
                        </h2>
                    </div>
                    <!-- Container -->
                    <div class="container team-members-wrap">
                        <h2 class="basic-text">
                            We are a small, passionate team. We love what we do, and that’s what makes our team special.<br>
                        </h2>
                        <br>
                        <br>
                        <br>
                        <!-- Team Members -->
                        <% for (Team p : az.com.Javanshir.Dao.listTeam("en")) {%>
                        <figure class="team-member four columns animated bounceInDown">
                            <img src="<%=p.getImage()%>" alt="Javel Group CEO Portrait">
                            <figcaption>
                                <div class="name-wrap">
                                    <p class="member-name"><%=p.getName()%></p>
                                    <p class="member-job"><%=p.getProfession()%></p>
                                </div>
                                <ul class="social-list">
                                    <li>
                                        <a href="<%=p.getFacebook()%>">
                                            <img src="files/icon-facebook.png" alt="">
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<%=p.getTwitter()%>">
                                            <img src="files/icon-twitter.png" alt="">
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<%=p.getLinkedin()%>">
                                            <img src="files/icon-linkedin.png" alt="">
                                        </a>
                                    </li>
                                </ul>
                            </figcaption>
                        </figure>
                        <% }%>
                        <!-- End Team Members -->
                    </div>
                    <!-- End Container -->
                    <!-- Container -->
                    <div class="container">
                        <h2 class="macro-block-heading-sub sixteen columns">
                            <span>Client Testimonials</span>
                        </h2>
                    </div>
                    <!-- End Container -->
                    <!-- Container -->
                    <div class="container testimonials-wrap animated rotateIn">
                        <div class="arrow-left one column"></div>
                        <!-- Testimonials -->
                        <div class="testimonials fourteen columns">
                            <!-- Flexslider -->
                            <div class="flexslider">
                                <ul class="slides">
                                    <!-- Slide 1 -->
                                    <li class="flex-active-slide" style="width: 100%; float: left; margin-right: -100%; position: relative; opacity: 1; display: block; z-index: 2;">
                                        <p class="testimonial-text">
                                            «Design must reflect the practical and aesthetic in business but above all... good design must primarily serve people.»
                                        </p>
                                        <p class="testimonial-author">
                                            - Thomas J. Watson
                                        </p>
                                    </li>
                                    <!-- End Slide 1 -->
                                    <!-- Slide 2 -->
                                    <li class="" style="width: 100%; float: left; margin-right: -100%; position: relative; opacity: 0; display: block; z-index: 1;">
                                        <p class="testimonial-text">
                                            «The preparation was excellent and no requirement was too much of a problem and the day provided exactly what we wanted»
                                        </p>
                                        <p class="testimonial-author">
                                            - John H. Smith
                                        </p>
                                    </li>
                                    <!-- End Slide 2 -->
                                </ul>
                            </div>
                            <!-- End Flexslider -->
                            <!-- Slider Pagination -->
                            <ul class="slider-pagination" id="testimonials-pagination">
                                <li class="flex-active"></li>
                                <li class=""></li>
                            </ul>
                            <!-- End Slider Pagination -->
                        </div>
                        <!-- End Testimonials -->
                        <div class="arrow-right one column"></div>
                    </div>
                    <!-- End Container -->
                </div>
                <!-- End Macro Block Content -->
            </section>
            <!-- End About Us -->
            <!-- Recent News -->
            <section id="news" class="news macro-block">
                <h1 class="macro-block-heading">
                    Recent News &amp; Events
                </h1>
                <!-- Macro Block Content -->
                <div class="macro-block-content">
                    <!-- Container -->
                    <div class="container recent-posts-wrap">
                        <!-- News Post 1 -->
                        <div class="one-third column news-post animated fadeInLeft">
                            <img src="files/recent-post-img-1.jpg" alt="">
                            <p class="content-heading-with-divider">
                                <span>HTML 5 &amp; CSS 3</span>
                            </p>
                            <p class="basic-text">
                                HTML5 and CSS3 have swept the web by storm in only 2 years. Before them there have been many altered semantics in the way web designers are expected to create web pages, and with their arrival come a slew of awesome supports such as alternative media, XML-style tags, and progressive input attributes for web designers to achieve dreamy features like animation.
                            </p>
                            <p class="date">
                                <span>October 16,2014</span>
                            </p>
                        </div>
                        <!-- End News Post 1 -->
                        <!-- News Post 2 -->
                        <div class="one-third column news-post animated fadeInDown delay-1s">
                            <img src="files/recent-post-img-2.jpg" alt="">
                            <p class="content-heading-with-divider">
                                <span>Responsive Design</span>
                            </p>
                            <p class="basic-text">
                                Another way to create a responsive design, is to use an already existing CSS framework. Bootstrap is the most popular HTML, CSS, and JS framework for developing responsive webs.
                            </p>
                            <p class="date">
                                <span>October 14,2014</span>
                            </p>
                        </div>
                        <!-- End News Post 2 -->
                        <!-- News Post 3 -->
                        <div class="one-third column news-post animated fadeInRight delay-0-5s">
                            <img src="files/recent-post-img-3.jpg" alt="">
                            <p class="content-heading-with-divider">
                                <span>Power of jQuery</span>
                            </p>
                            <p class="basic-text">
                                As the web evolves, new technologies are emerging and uniting in remarkable ways. The combination of Ajax and jQuery, in particular, is one of the most powerful unions to date.
                            </p>
                            <p class="date">
                                <span>October 11,2014</span>
                            </p>
                        </div>
                        <!-- End News Post 3 -->
                    </div>
                    <!-- End Container -->
                </div>  
                <!-- End Macro Block Content -->
            </section>
            <!-- End Recent News -->
            <!-- Contact -->
            <section id="contact" class="contact macro-block">
                <h1 class="macro-block-heading">
                    Contact Us
                </h1>
                <!-- Macro Block Content -->
                <div class="macro-block-content">
                    <!-- Container -->
                    <div class="container">
                        <!-- Contact details -->
                        <div class="four columns contact-details-wrap animated rotateInDownLeft">
                            <h2 class="content-heading">
                                Contact Details
                            </h2>
                            <ul class="contact-details-list">
                                <li class="phone-icon">
                                    <p class="basic-text bold">Phone</p>
                                    <p class="basic-text">+48(53)530-28-86</p>
                                </li>
                                <li class="email-icon">
                                    <p class="basic-text bold">E-mail</p>
                                    <p class="basic-text">your-mail@gmail.com</p>
                                </li>
                            </ul>
                        </div>
                        <!-- End Contact details -->
                        <!-- Contact Form -->
                        <div class="contact-form twelve columns animated rotateInUpRight delay-0-5s">
                            <h2 class="content-heading">Send a Message</h2>
                            <form action="post" class="twelve columns alpha omega">
                                <input type="text" value="Your name" id="name" name="name" class="four columns alpha">
                                <input type="text" value="Your e-mail" id="email" name="email" class="four columns">
                                <input type="text" value="Your website*" id="website" name="website" class="four columns omega">
                                <textarea id="message" name="message" class="twelve columns alpha omega">Your message goes here</textarea>
                                <p class="note six columns alpha">
                                    * optional fields
                                </p>
                                <div class="submit six columns omega">
                                    <button type="submit" id="submit" name="submit">
                                        <img src="files/send-button.png" alt="">
                                    </button>
                                </div>
                            </form>
                        </div>
                        <!-- End Contact Form -->
                    </div>
                    <!-- End Container -->
                    <!-- Social Links -->
                    <div class="social-links container">
                        <h2 class="macro-block-heading-sub sixteen columns">
                            <span>Stay Connected</span>
                        </h2>
                        <!-- Link 1 -->
                        <div class="four columns animated fadeInUp delay-0-5s">
                            <a href="https://twitter.com/JavelGroup2014" class="square-link two columns alpha">
                                <img src="files/icon-twitter-large.png" alt="">
                            </a>
                            <p class="thin-text two columns omega">
                                Follow Us on Twitter
                            </p>
                        </div>
                        <!-- End Link 1 -->
                        <!-- Link 2 -->
                        <div class="four columns animated fadeInUp delay-0-5s">
                            <a href="https://www.facebook.com/groupjavel" class="square-link two columns alpha">
                                <img src="files/icon-facebook-large.png" alt="">
                            </a>
                            <p class="thin-text two columns omega">
                                Follow Us on Facebook
                            </p>
                        </div>
                        <!-- End Link 2 -->
                        <!-- Link 3 -->
                        <div class="four columns animated fadeInUp delay-0-5s">
                            <a href="http://vk.com/javel_group" class="square-link two columns alpha">
                                <img src="files/icon-vk-large.png" alt="">
                            </a>
                            <p class="thin-text two columns omega">
                                Follow Us on VKontakte
                            </p>
                        </div>
                        <!-- End Link 3 -->
                        <!-- Link 4 -->
                        <div class="four columns animated fadeInUp delay-0-5s">
                            <a href="#" class="square-link two columns alpha">
                                <img src="files/icon-rss-large.png" alt="">
                            </a>
                            <p class="thin-text two columns omega">
                                Get Updates Via RSS
                            </p>
                        </div>
                        <!-- End Link 4 -->
                    </div>
                    <!-- End Social Links & Container -->
                </div>  
                <!-- End Macro Block Content -->
            </section>
            <!-- End Contact -->
        </div>
        <!-- End Main Page Content -->
        <!-- Site Footer -->
        <footer id="site-footer" class="site-footer">
            <!-- Large Container -->
            <div class="large-container">
                <!-- Left Element -->
                <div class="left-element">
                    <a href="http://javelgroup.org">
                        Copyright 2014-2015 Javel Group
                    </a>
                </div>
                <!-- End Left Element -->
                <!-- Right Element -->
                <div class="right-element">
                    <a href="#">
                        <!-- Legal Information -->
                    </a>
                </div>
                <!-- End Right Element -->
            </div>
            <!-- End Large Container -->
        </footer>
        <!-- End Site Footer -->
        <!-- Go to Top Button -->
        <a class="go-to-top" href="#"></a>
        <!-- End Go to Top Button -->
        <!-- Navigation for Smartphones -->
        <div id="modal">
            <nav>
                <ul>
                    <li>
                        <a href="#" class="home-link">Home</a>
                    </li>
                    <li>
                        <a href="#portfolio">Portfolio</a>
                    </li>
                    <li>
                        <a href="#services">Services</a>
                    </li>
                    <li>
                        <a href="#about-us">About</a>
                    </li>
                    <li>
                        <a href="#news">News</a>
                    </li>
                    <li>
                        <a href="#contact">Contact</a>
                    </li>
                    <li>
                        <a href="#" class="close-nav">X Close Navigation</a>
                    </li>
                </ul>
            </nav>
        </div>
        <!-- End Navigation for Smartphones -->
        <!-- Scripts -->
        <script src="files/scripts-before-closing-body.js"></script><div id="pageslide" style="display: none;"></div>
        <a id="the-trigger" href="#" style="display: none;"></a>
    </body>
</html>