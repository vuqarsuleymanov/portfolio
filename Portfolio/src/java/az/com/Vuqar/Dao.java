package az.com.Vuqar;

import az.com.entity.Portfolio;
import com.mysql.jdbc.Driver;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author MacBook
 */
public class Dao {

    private static Connection conn = null;
    private static PreparedStatement pst = null;
    private static ResultSet rs = null;

    public static void connect() {
        try {
            DriverManager.registerDriver(new Driver());
            conn = DriverManager.getConnection("jdbc:mysql://62.141.49.103:3306/portfolio?characterEncoding=UTF-8", "vuqar", "admin");
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        }
    }

    public static void close() {
        try {
            pst.close();
            conn.close();
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        }
    }

    public static List<Portfolio> listPortfolio(String lang) {
        try {

            List<Portfolio> list = new ArrayList<Portfolio>();
            String sql = "select id, description_" + lang + ", image, type from portfolio";
            connect();
            pst = conn.prepareStatement(sql);
            rs = pst.executeQuery();
            while (rs.next()) {
                Portfolio p = new Portfolio();
                p.setId(rs.getInt(1));
                p.setDescription(rs.getString(2));
                p.setImage(rs.getString(3));
                p.setType(rs.getString(4));
                list.add(p);
            }
            return list;
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
            return null;
        } finally {
            close();
        }
    }

    public static void main(String[] args) {
        for (Portfolio p : Dao.listPortfolio("az")) {
            System.out.println(p.getDescription());
        }
    }
}
