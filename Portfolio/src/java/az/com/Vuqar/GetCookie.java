package az.com.Vuqar;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "GetCookie", urlPatterns = {"/GetCookie"})
public class GetCookie extends HttpServlet {

    public static String getCookieByName(Cookie[] cookies, String name) {
        String c = null;

        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals(name)) {
                    c = cookie.getValue();
                }
            }
        }

        return c;
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Cookie cookie;
        if (GetCookie.getCookieByName(request.getCookies(), "lang") == null) {
            cookie = new Cookie("lang", "az");
            cookie.setMaxAge(-1);
            response.addCookie(cookie);

        }
        response.sendRedirect("index.jsp");
    }

}
