/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package az.com.Rufan;

import com.mysql.jdbc.Driver;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author MacBook
 */
public class Dao {
    private static Connection conn = null;
    private static PreparedStatement pst = null;
    private static ResultSet rs = null;
    
    public static void connect(){
        try {
            DriverManager.registerDriver(new Driver());
            conn = DriverManager.getConnection("jdbc:mysql://62.141.49.103:3306/portfolio?characterEncoding=UTF-8", "vuqar", "admin");
        } catch (SQLException ex) {
            Logger.getLogger(az.com.Javanshir.Dao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void close(){
        try {
            pst.close();
            conn.close();
        } catch (SQLException ex) {
            Logger.getLogger(az.com.Javanshir.Dao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
