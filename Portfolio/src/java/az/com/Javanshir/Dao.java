package az.com.Javanshir;

import az.com.entity.About;
import az.com.entity.Projects;
import az.com.entity.Services;
import az.com.entity.Team;
import com.mysql.jdbc.Driver;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

//@author Javanshir
public class Dao {

    private static Connection conn = null;
    private static PreparedStatement pst = null;
    private static ResultSet rs = null;

    public static void connect() {
        try {
            DriverManager.registerDriver(new Driver());
            conn = DriverManager.getConnection("jdbc:mysql://62.141.49.103:3306/portfolio?characterEncoding=UTF-8", "vuqar", "admin");
        } catch (SQLException ex) {
            Logger.getLogger(Dao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void close() {
        try {
            pst.close();
            conn.close();
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        }
    }

    public static List<Team> listTeam(String lang) {
        try {
            List<Team> list = new ArrayList<Team>();
            String sql = "select id, name, profession, facebook, twitter, linkedin, image from team";
            connect();
            pst = conn.prepareStatement(sql);
            rs = pst.executeQuery();
            while (rs.next()) {
                Team p = new Team();
                p.setId(rs.getInt(1));
                p.setName(rs.getString(2));
                p.setProfession(rs.getString(3));
                p.setFacebook(rs.getString(4));
                p.setTwitter(rs.getString(5));
                p.setLinkedin(rs.getString(6));
                p.setImage(rs.getString(7));
                list.add(p);
            }
            return list;
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
            return null;
        } finally {
            close();
        }
    }

    public static List<Projects> listProjects(String lang) {
        try {
            List<Projects> list = new ArrayList<Projects>();
            String sql = "select id, project_name, img, video, description from projects";
            connect();
            pst = conn.prepareStatement(sql);
            rs = pst.executeQuery();
            while (rs.next()) {
                Projects p = new Projects();
                p.setId(rs.getInt(1));
                p.setProject_name(rs.getString(2));
                p.setImg(rs.getString(3));
                p.setVideo(rs.getString(4));
                p.setDescription(rs.getString(5));
                list.add(p);
            }
            return list;
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
            return null;
        } finally {
            close();
        }
    }

    public static List<About> listAbout(String lang) {
        try {
            List<About> list = new ArrayList<About>();
            String sql = "select id, title_" + lang + ", description_" + lang + " from about";
            connect();
            pst = conn.prepareStatement(sql);
            rs = pst.executeQuery();
            while (rs.next()) {
                About p = new About();
                p.setId(rs.getInt(1));
                p.setTitle(rs.getString(2));
                p.setDescription(rs.getString(3));
                list.add(p);
            }
            return list;
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
            return null;
        } finally {
            close();
        }
    }

    public static List<Services> listServices(String lang) {
        try {
            List<Services> list = new ArrayList<Services>();
            String sql = "select id, icon, service_name_" + lang + ", description_" + lang + " from services";
            connect();
            pst = conn.prepareStatement(sql);
            rs = pst.executeQuery();
            while (rs.next()) {
                Services p = new Services();
                p.setId(rs.getInt(1));
                p.setIcon(rs.getString(2));
                p.setService_name(rs.getString(3));
                p.setDescription(rs.getString(4));
                list.add(p);
            }
            return list;
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
            return null;
        } finally {
            close();
        }
    }

    public static void main(String[] args) {
        for (Services p : az.com.Javanshir.Dao.listServices("en")) {
            System.out.println(p.getDescription());
        }
    }
}
