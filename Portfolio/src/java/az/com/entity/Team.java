package az.com.entity;

//@author javanshir
public class Team {

    private int id;
    private String name;
    private String profession;
    private String facebook;
    private String twitter;
    private String linkedin;
    private String image;

    public Team(int id, String name, String profession, String facebook, String twitter, String linkedin, String image) {
        this.id = id;
        this.name = name;
        this.profession = profession;
        this.facebook = facebook;
        this.twitter = twitter;
        this.linkedin = linkedin;
        this.image = image;
    }

    public Team() {
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getProfession() {
        return profession;
    }

    public String getFacebook() {
        return facebook;
    }

    public String getTwitter() {
        return twitter;
    }

    public String getLinkedin() {
        return linkedin;
    }

    public String getImage() {
        return image;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setProfession(String profession) {
        this.profession = profession;
    }

    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }

    public void setTwitter(String twitter) {
        this.twitter = twitter;
    }

    public void setLinkedin(String linkedin) {
        this.linkedin = linkedin;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
