package az.com.entity;

//@author javanshir
public class Projects {

    private int id;
    private String project_name;
    private String img;
    private String video;
    private String description;

    public Projects() {
    }

    public Projects(int id, String project_name, String img, String video, String description) {
        this.id = id;
        this.project_name = project_name;
        this.img = img;
        this.video = video;
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public String getProject_name() {
        return project_name;
    }

    public String getImg() {
        return img;
    }

    public String getVideo() {
        return video;
    }

    public String getDescription() {
        return description;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setProject_name(String project_name) {
        this.project_name = project_name;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
