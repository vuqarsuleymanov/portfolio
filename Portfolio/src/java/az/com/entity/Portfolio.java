package az.com.entity;

import java.io.Serializable;

/**
 *
 * @author MacBook
 */
public class Portfolio implements Serializable{
    private int id;
    private String image;
    private String description;
    private String type;

    public Portfolio() {
    }

    public Portfolio(int id, String image, String description, String type) {
        this.id = id;
        this.image = image;
        this.description = description;
        this.type = type;
    }

    public final int getId() {
        return id;
    }

    public final void setId(int id) {
        this.id = id;
    }

    public final String getImage() {
        return image;
    }

    public final void setImage(String image) {
        this.image = image;
    }

    public final String getDescription() {
        return description;
    }

    public final void setDescription(String description) {
        this.description = description;
    }

    public final String getType() {
        return type;
    }

    public final void setType(String type) {
        this.type = type;
    }
    
    
}
