package az.com.entity;

//@author javanshir
public class Services {

    private int id;
    private String icon;
    private String service_name;
    private String description;

    public Services() {
    }

    public Services(int id, String icon, String service_name, String description) {
        this.id = id;
        this.icon = icon;
        this.service_name = service_name;
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public String getIcon() {
        return icon;
    }

    public String getService_name() {
        return service_name;
    }

    public String getDescription() {
        return description;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public void setService_name(String service_name) {
        this.service_name = service_name;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
